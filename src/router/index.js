import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const commonRoutes = [
    {
        path: '/login',
        name: 'login',
        component: () => import('../components/Login.vue')
    },
    {path: '/', redirect: '/home'},
] 

// 需要通过后台数据来生成的组件
export const asyncRoutes = {
    'home': {
        path: 'home',
        name: 'home',
        component: () => import('../views/Home.vue')
    },
    'user': {
        path: 'user',
        name: 'user',
        component: () => import('../views/User.vue')
    },
    't1': {
        path: 't1',
        name: 't1',
        component: () => import('../views/T1.vue')
    },
    'password': {
        path: 'password',
        name: 'password',
        component: () => import('../views/Password.vue')
    },
    'msg': {
        path: 'msg',
        name: 'msg',
        component: () => import('../views/Msg.vue')
    },
    'user-device': {
        path: 'user-device',
        name: 'user-device',
        component: () => import('../views/UserDevice.vue')
    },
    'device': {
        path: 'device',
        name: 'device',
        component: () => import('../views/Device.vue')
    },
    'promission': {
        path: 'promission',
        name: 'promission',
        component: () => import('../views/Promission.vue')
    },
    'managerinfo': {
        path: 'managerinfo',
        name: 'managerinfo',
        component: () => import('../views/ManagerInfo.vue')
    },
    'systemuser': {
        path: 'systemuser',
        name: 'systemuser',
        component: () => import('../views/Manager.vue')
    }
}

const createRouter = () => new Router({
    routes: commonRoutes
})

const router = createRouter()

export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher 
}

export default router