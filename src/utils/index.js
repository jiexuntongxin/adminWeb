import { asyncRoutes, resetRouter } from '../router'

// 将菜单信息转成对应的路由信息 动态添加
export function menusToRoutes(data) {
    const result = []
    const children = []

    result.push({
        path: '/',
        component: () => import('../components/Index.vue'),
        children,
    })

    data.forEach(item => {
        generateRoutes(children, item)
    })

    children.push({
        path: 'error',
        name: 'error',
        component: () => import('../components/Error.vue')
    })

    // 最后添加404页面 否则会在登陆成功后跳到404页面
    result.push(
        {path: '*', redirect: '/error'},
    )

    return result
}

function generateRoutes(children, item) {
    if (item.name) {
        children.push(asyncRoutes[item.name])
    } else if (item.children) {
        item.children.forEach(e => {
            generateRoutes(children, e)
        })
    }
}

export function resetTokenAndClearUser() {
    // 退出登陆 清除用户资料
    localStorage.removeItem('ManagerToken')
    localStorage.removeItem('LoginManager')
    // 重设路由
    resetRouter()
}

/**
 * 时间格式化
 */
export function formatDate (date, fmt) {
    if(!fmt)
        fmt = 'yyyy-MM-dd hh:mm:ss'

    date = new Date(date)
    
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
    }
    let o = {
      'M+': date.getMonth() + 1,
      'd+': date.getDate(),
      'h+': date.getHours(),
      'm+': date.getMinutes(),
      's+': date.getSeconds()
    }
    for (let k in o) {
      if (new RegExp(`(${k})`).test(fmt)) {
        let str = o[k] + ''
        fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 1 ? str : padLeftZero(str))
      }
    }
    return fmt
  }
  
  function padLeftZero (str) {
    return ('00' + str).substr(str.length)
  }

  export default { formatDate, } 