import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        // 左侧菜单栏数据
        menuItems: [
            {
                name: 'home', // 要跳转的路由名称 不是路径
                size: 18, // icon大小
                type: 'md-home', // icon类型
                text: '主页' // 文本内容
            },
            // {
            //     text: '二级菜单',
            //     type: 'ios-paper',
            //     children: [
            //         {
            //             type: 'ios-grid',
            //             name: 't1',
            //             text: '表格'
            //         },
            //         {
            //             text: '三级菜单',
            //             type: 'ios-paper',
            //             children: [
            //                 {
            //                     type: 'ios-notifications-outline',
            //                     name: 'msg',
            //                     text: '查看消息'
            //                 },
            //                 {
            //                     type: 'md-lock',
            //                     name: 'password',
            //                     text: '修改密码'
            //                 },
            //                 {
            //                     type: 'md-person',
            //                     name: 'userinfo',
            //                     text: '基本资料',
            //                 }
            //             ]
            //         }
            //     ]
            // },
            {
                text: '用户管理',
                type: 'md-person',
                children: [
                    {
                        type: 'md-person',
                        name: 'user',
                        text: '用户管理'
                    }
                ]
            },
            {
                text: '设备管理',
                type: 'md-videocam',
                children: [
                    {
                        type: 'ios-cube',
                        name: 'user-device',
                        text: '用户设备'
                    }
                    ,
                    {
                        type: 'md-videocam',
                        name: 'device',
                        text: '商品设备'
                    }
                ]
            },
            {
                text: '系统管理',
                type: 'md-settings',
                children: [
                    {
                        type: 'md-person',
                        name: 'systemuser',
                        text: '账号管理'
                    },
                    {
                        type: 'ios-notifications-outline',
                        name: 'msg',
                        text: '查看消息'
                    },
                    {
                        type: 'md-lock',
                        name: 'password',
                        text: '修改密码'
                    },
                    {
                        type: 'md-person',
                        name: 'managerinfo',
                        text: '基本资料',
                    }
                ]
            }
        ],
    },
    mutations: {
        setMenus(state, items) {
            state.menuItems = [...items]
        },
    }
})

export default store